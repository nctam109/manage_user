def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('add_view', '/add')
    config.add_route('edit_user', '/edit-user')

    # REST API
    config.add_route('users', '/api/user')
    config.add_route('user', '/api/user/{username}')
