from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound

from sqlalchemy.exc import DBAPIError

from ..models import User


@view_config(route_name='home', renderer='../templates/mytemplate.jinja2')
def my_view(request):
    try:
        users = request.dbsession.query(User)
    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return {'users': 'aaa'}


@view_config(route_name='add_view', renderer='../templates/add-user.jinja2')
def add_user_view(request):

    if request.method == 'POST':
        newUserName = request.POST["username"]
        newPwd = request.POST["pwd"]
        newFullname = request.POST["fullname"]
        newAge = request.POST["age"]
        newSex = request.POST["sexradio"]

        if newSex == 'Male':
            newSex = 'M'
        else:
            newSex = 'F'

        try:
            user = request.dbsession.query(User).filter(User.user_name == newUserName).first()
            if user:
                user.pass_word = newPwd
                user.full_name = newFullname
                user.age = newAge
                user.sex = newSex
                request.dbsession.flush()
            else:
                newUser = User(user_name=newUserName, pass_word=newPwd, full_name=newFullname, age=newAge, sex=newSex)
                request.dbsession.add(newUser)
        except Exception as e:
            print e
        except DBAPIError as e:
            return Response(db_err_msg, content_type='text/plain', status=500)
        finally:
            url = request.route_url('home')
            return HTTPFound(url)

    return {"data": {"name": "A new form!"}}


@view_config(route_name='edit_user', renderer='../templates/add-user.jinja2')
def edit_user_view(request):

    return {"user": "user"}


@view_config(route_name='users', renderer='json', request_method='GET')
def all_user(request):
    try:
        users = request.dbsession.query(User)
        users_list = []
        for user in users:
            users_list.append({
                "username": user.user_name,
                "fullname": user.full_name,
                "age": user.age,
                "sex": user.sex
            })
            print(user.user_name + '\t' + user.full_name)
    except DBAPIError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return {'users': users_list}


@view_config(route_name='user', renderer='json', request_method='GET')
def user_by_username(request):
    username = request.matchdict["username"]
    try:
        user = request.dbsession.query(User).filter(User.user_name == username).first()
        result = {}
        if user:
            result = {
                "username": user.user_name,
                "password": user.pass_word,
                "fullname": user.full_name,
                "age": user.age,
                "sex": user.sex
            }
    except Exception as e:
        print e
    return {'user': result}


@view_config(route_name='users', renderer='json', request_method='POST')
def add_user(request):
    username = request.json_body.get("username")
    result = True
    try:
        user = request.dbsession.query(User).filter(User.user_name == username).first()
        if user is None:
            password = request.json_body.get("password")
            fullname = request.json_body.get("fullname")
            age = request.json_body.get("age")
            sex = request.json_body.get("sex")
            if sex == 'Male':
                sex = 'M'
            else:
                sex = 'F'
            params = {
                'user_name': username,
                'pass_word': password,
                'full_name': fullname,
                'age': age,
                'sex': sex
            }
            user = User(**params)
            request.dbsession.add(user)
    except Exception as e:
        print e
        result = False
    return {'success': result}


@view_config(route_name='user', renderer='json', request_method='PUT')
def edit_user(request):
    username = request.json_body.get("username")
    result = True
    try:
        user = request.dbsession.query(User).filter(User.user_name == username).first()
        if user:
            user.pass_word = request.json_body.get("password")
            user.full_name = request.json_body.get("fullname")
            user.age = request.json_body.get("age")
            sex = request.json_body.get("sex")
            if sex == 'Male':
                user.sex = 'M'
            else:
                user.sex = 'F'
            request.dbsession.flush()
    except Exception as e:
        print e
        result = False
    return {'success': result}


@view_config(route_name='user', renderer='json', request_method='DELETE')
def delete_user(request):
    username = request.matchdict["username"]
    result = True
    try:
        user = request.dbsession.query(User).filter(User.user_name == username).first()
        if user:
            request.dbsession.delete(user)
    except Exception as e:
        result = False
        print e
    return {'result': result}


db_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to run the "initialize_manage_user2_db" script
    to initialize your database tables.  Check your virtual
    environment's "bin" directory for this script and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""
